using Microsoft.AspNetCore.Mvc;
using payment_api.Services;
using payment_api.models;
using payment_api.models.enums;
using payment_api.Services.Exceptions;

namespace payment_api.controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
       private readonly VendasServices _service;

        public VendasController(VendasServices service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> RegistrarVenda(Venda venda)
        {  
            try
            {
                Venda Venda = await _service.RegistrarVenda(venda);
                return CreatedAtAction(nameof(BuscarVenda), new {id = venda.VendaId}, venda);
            }
            catch(DomainExceptions e)
            {
                System.Console.WriteLine("Erro:" + e );
                return Ok("Erro: " + e);
            }
        }

        [HttpGet]
        public async Task<IActionResult> BuscarVenda(int vendaId)
        {
            try
            {
                Venda venda = await _service.BuscarVenda(vendaId);
                return Ok(venda);
            }
            catch(DomainExceptions e)
            {
                System.Console.WriteLine("Erro:" + e );
                return Ok("Erro: " + e);
            }
        }

        [HttpPatch]
        public async Task<IActionResult> AtualizarVenda(int vendaId, StatusVenda status)
        {
            try{
                Venda VendaAlterada = await _service.AtualizarVenda(vendaId, status);
                return Ok(VendaAlterada);
            }
            catch(DomainExceptions e)
            {
                System.Console.WriteLine("Erro:" + e );
                return Ok("Erro: " + e);
            }
        }
    }
}