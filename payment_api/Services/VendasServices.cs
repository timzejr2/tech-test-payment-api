using payment_api.context;
using payment_api.models;
using payment_api.models.enums;
using payment_api.Services.Exceptions;

namespace payment_api.Services
{
    public class VendasServices
    {
        private readonly VendaContext _context;

        public VendasServices(VendaContext context)
        {
            _context = context;
        }

        public VendasServices()
        {
        }

        public async Task<Venda> RegistrarVenda(Venda venda)
        {

            Venda Venda = venda;
            
            Vendedor Vendedor = venda.Vendedor;

            if(Vendedor == null || Vendedor.NomeVendedor == "") throw new DomainExceptions("A venda deve possuir um vendedor.");

            _context.Add(Vendedor);

            Venda.DataVenda = DateTime.Now;
            Venda.Status = 0;
            _context.Add(Venda);

            _context.Add(Vendedor);

            ICollection<ItemVenda> ItemVenda = venda.ItemVenda.ToList();

            if(ItemVenda.Count() < 1) throw new DomainExceptions("A venda deve possuir pelo menos um item.");

            foreach(ItemVenda item in ItemVenda)
            {
                await _context.AddAsync(item);

                Produto Produto = item.Produto;
                await _context.AddAsync(Produto);
            }

            await _context.SaveChangesAsync();

            return Venda;
        }

        public async Task<Venda> BuscarVenda(int vendaId)
        {

            Venda venda = await _context.Vendas.FindAsync(vendaId);

            if(venda == null) throw new DomainExceptions("Venda não encontrada!");

            Vendedor vendedor = await _context.Vendedores.FindAsync(venda.VendedorId);

            venda.Vendedor = vendedor;
            IQueryable<ItemVenda> itensVenda = _context.ItensVendas.Where(x => x.VendaId == vendaId);

            ICollection<ItemVenda> itens = itensVenda.ToList();
            foreach(ItemVenda item in itens)
            {
                item.Produto = await _context.Produto.FindAsync(item.ProdutoId);
            }

            venda.ItemVenda = itens;

            return venda;
        }

        public async Task<Venda> AtualizarVenda(int vendaId, StatusVenda status)
        {
            Venda venda = await _context.Vendas.FindAsync(vendaId);

            if(venda == null) throw new DomainExceptions("Venda não encontrada!");

            List<string> statusVenda = new List<string>()
            {
                "Aguardando_Pagamento",
                "Pagamento_aprovado",
                "Enviado_para_transportadora",
                "Entregue",
                "Cancelada"
            };

            if(statusVenda.IndexOf(venda.Status.ToString()) == 0 && statusVenda.IndexOf(status.ToString()) == 1 || statusVenda.IndexOf(venda.Status.ToString()) == 0 && statusVenda.IndexOf(status.ToString()) == 4)
            {
                venda.Status = status;
            }
            else if(statusVenda.IndexOf(venda.Status.ToString()) == 1 && statusVenda.IndexOf(status.ToString()) == 2 || statusVenda.IndexOf(venda.Status.ToString()) == 1 && statusVenda.IndexOf(status.ToString()) == 4)
            {
                venda.Status = status;
            }
            else if(statusVenda.IndexOf(venda.Status.ToString()) == 2 && statusVenda.IndexOf(status.ToString()) == 3)
            {
                venda.Status = status;
            }
            else
            {
                throw new Exception("Alteração não permitida");
            }

            await _context.SaveChangesAsync();

            return venda;
        }
    }
}