namespace payment_api.models.enums
{
    public enum StatusVenda
    {
        Aguardando_Pagamento,
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelada
    }
}