namespace payment_api.models
{
    public class Vendedor
    {
        public virtual int VendedorId { get; set; }
        public string Cpf { get; set; }
        public string NomeVendedor { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}