namespace payment_api.models
{
    public class Produto
    {
        public virtual int ProdutoId { get; set; }
        public string NomeProduto { get; set; }
        public decimal PrecoProduto { get; set; }

    }
}