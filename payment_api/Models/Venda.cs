using payment_api.models.enums;

namespace payment_api.models
{
    public class Venda
    {
        public int VendaId { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public ICollection<ItemVenda> ItemVenda { get; set; } = new List<ItemVenda>();
        public DateTime DataVenda { get; set; }
        public StatusVenda Status { get; set; }

        internal Task<Venda> toListAsync()
        {
            throw new NotImplementedException();
        }
    }
}