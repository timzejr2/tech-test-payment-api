using payment_api.models;

namespace payment_api.Interfaces
{
    public interface IVendasServices
    {
        Venda RegistrarVenda(Venda venda);
    }
}